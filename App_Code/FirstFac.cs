﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FirstFac
/// </summary>
public class FirstFac
{
    dbAccess objData = new dbAccess();
    SqlCommand CMD = new SqlCommand();


    public DataTable FindUser(string _username, string _password)
    {
        CMD = new SqlCommand("SELECT fldName, fldAdress, fldUserLevel, fldUserId FROM CL_Users WHERE fldName = @name AND fldAdress = @adress");
        CMD.Parameters.AddWithValue("@name", _username);
        CMD.Parameters.AddWithValue("@adress", _password);

        return objData.GetData(CMD);
    }
    public DataRow FindUserLog(int _id)
    {
        CMD = new SqlCommand("SELECT * FROM CL_Users WHERE fldUserId = @id");
        CMD.Parameters.AddWithValue("@id", _id);

        return objData.GetData(CMD).Rows[0];
    }
    public void setActive()
    {
        int active = 1;
        CMD = new SqlCommand("UPDATE CL_AdminSession SET fldActiveStatus = " + active);

        objData.ModifyData(CMD);
    }
    public void setDeactive()
    {
        int active = 0;
        CMD = new SqlCommand("UPDATE CL_AdminSession SET fldActiveStatus = " + active);

        objData.ModifyData(CMD);
    }
    public DataRow CheckActive()
    {
        CMD = new SqlCommand("SELECT fldActiveStatus FROM CL_AdminSession");

        return objData.GetData(CMD).Rows[0];
    }
    public void AddUser(string _name, string _adress, string _email, int _postalcode)
    {
        CMD = new SqlCommand("INSERT INTO CL_Users (fldName, fldAdress, fldEmail, fldPostalCode) VALUES (@Name, @Adress, @Email, @PostalCode)");
        CMD.Parameters.AddWithValue("@Name", _name);
        CMD.Parameters.AddWithValue("@Adress", _adress);
        CMD.Parameters.AddWithValue("@Email", _email);
        CMD.Parameters.AddWithValue("@Postalcode", _postalcode);
        objData.ModifyData(CMD);
    }
    public void CreateUserLog(int _userid)
    {
        CMD = new SqlCommand("INSERT INTO CL_Log (fldUserId_fk, fldStartTime, fldStatus, fldEndTime) VALUES (@id, @starttime, @status, @endtime)");
        CMD.Parameters.AddWithValue("@id", _userid);
        CMD.Parameters.AddWithValue("@starttime", DateTime.Now);
        CMD.Parameters.AddWithValue("@endtime", DateTime.Today.ToString("yyyy-MM-dd 23:59:00"));
        CMD.Parameters.AddWithValue("@status", 0);

        objData.ModifyData(CMD);
    }
    public void EndUserLog(int _userid)
    {
        CMD = new SqlCommand("UPDATE CL_Log SET fldEndTime = @endtime, fldStatus = @status WHERE fldUserId_fk = @id AND fldStatus = @activestatus");
        CMD.Parameters.AddWithValue("@id", _userid);
        CMD.Parameters.AddWithValue("@endtime", DateTime.Now);
        CMD.Parameters.AddWithValue("@status", 1);
        CMD.Parameters.AddWithValue("@activestatus", 0);
        CMD.Parameters.AddWithValue("@starttime", DateTime.Now);

        objData.ModifyData(CMD);
    }
    public void UserNotLoggetOut(int _userid)
    {
        CMD = new SqlCommand("UPDATE CL_Log SET fldStatus = @status WHERE fldUserId_fk = @id AND fldStatus = @activestatus");
        CMD.Parameters.AddWithValue("@id", _userid);
        CMD.Parameters.AddWithValue("@status", 2);
        CMD.Parameters.AddWithValue("@activestatus", 0);

        objData.ModifyData(CMD);
    }
    public DataTable UserLogWithNul(int _userId)
    {
        CMD = new SqlCommand("SELECT * FROM CL_Log WHERE fldUserId_fk = @id AND fldStatus = @status");
        CMD.Parameters.AddWithValue("@id", _userId);
        CMD.Parameters.AddWithValue("@status", 0);
        return objData.GetData(CMD);
    }
    public DataTable UserLogs(int _userId)
    {
        CMD = new SqlCommand("SELECT * FROM CL_Log WHERE fldUserId_fk = @id ORDER BY fldStartTime DESC");
        CMD.Parameters.AddWithValue("@id", _userId);

        return objData.GetData(CMD);
    }

    public DataTable Users()
    {
        CMD = new SqlCommand("SELECT * FROM CL_Users WHERE fldUserLevel = @lvl");
        CMD.Parameters.AddWithValue("@lvl", 0);
        return objData.GetData(CMD);
    }
}