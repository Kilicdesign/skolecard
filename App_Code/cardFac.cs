﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for cardFac
/// </summary>
public class cardFac
{
    dbAccess _conn = new dbAccess();
    SqlCommand _cmd = new SqlCommand();

    public int _userid { get; set; }
    public string _name { get; set; }
    public string _adress { get; set; }
    public int _postal { get; set; }
    public string _email { get; set; }
    public int _userlevel { get; set; }
    public int _logid { get; set; }
    public int _useridfk { get; set; }
    public int _starttime { get; set; }
    public int _endtime { get; set; }
    public int _status { get; set; }
    public int _adminsessionid { get; set; }
    public int _adminidfk { get; set; }
    public int _adminstatus { get; set; }

    //public DataTable RegisterUser()
    //{
    //    _cmd = new SqlCommand("INSERT INTO CL_Users (fldUserId, fldName, fldAdress, fldPostalCode, fldEmail, fldUserLevel) VALUES (@userid, @name, @adress, @postal, @email, @userlevel)");
    //    _cmd.Parameters.AddWithValue("@userid", _userid);
    //    _cmd.Parameters.AddWithValue("@name", _name);
    //    _cmd.Parameters.AddWithValue("@adress", _adress);
    //    _cmd.Parameters.AddWithValue("@postal", _postal);
    //    _cmd.Parameters.AddWithValue("@email", _email);
    //    _cmd.Parameters.AddWithValue("@userlevel", _userlevel);

    //    _conn.ModifyData(_cmd);
    //}

    //public DataTable CardLogIn()
    //{
    //    _cmd = new SqlCommand("INSERT INTO CL_Log (fldLogId, fldUserId_fk, fldStartTime, fldStatus) VALUES (@logid, @useridfk, @starttime, @status)");
    //    _cmd.Parameters.AddWithValue("@logid", _logid);
    //    _cmd.Parameters.AddWithValue("@useridfk", _useridfk);
    //    _cmd.Parameters.AddWithValue("@starttime", _starttime);
    //    _cmd.Parameters.AddWithValue("@status", _status);

    //    _conn.ModifyData(_cmd);
    //}

    //public DataTable CardLogOut()
    //{
    //    _cmd = new SqlCommand("INSERT INTO CL_Log (fldLogId, fldUserId_fk, fldEndTime, fldStatus) VALUES (@logid, @useridfk, @endtime, @status)");
    //    _cmd.Parameters.AddWithValue("@logid", _logid);
    //    _cmd.Parameters.AddWithValue("@useridfk", _useridfk);
    //    _cmd.Parameters.AddWithValue("@endtime", _endtime);
    //    _cmd.Parameters.AddWithValue("@status", _status);

    //    _conn.ModifyData(_cmd);
    //}

    //public DataTable AdminSession()
    //{
    //    _cmd = new SqlCommand("INSERT INTO CL_AdminSession (fldAdminSessionId, fldAdminId_fk, fldActiveStatus) VALUES (@adminsesionid, @adminidfk, @adminstatus)");
    //    _cmd.Parameters.AddWithValue("@adminsessionid", _adminsessionid);
    //    _cmd.Parameters.AddWithValue("@adminidfk", _adminidfk);
    //    _cmd.Parameters.AddWithValue("@adminstatus", _adminstatus);

    //    _conn.ModifyData(_cmd);
    //}
}