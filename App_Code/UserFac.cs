﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserFac
/// </summary>
public class UserFac
{
    dbAccess objData = new dbAccess();
    SqlCommand CMD = new SqlCommand();

    public void AddUser(string _name, string _adress, string _email, int _postalcode)
    {
        CMD = new SqlCommand("INSERT INTO CL_Users (fldName, fldAdress, fldEmail, fldPostalCode) VALUES (@Name, @Adress, @Email, @PostalCode)");
        CMD.Parameters.AddWithValue("@Name", _name);
        CMD.Parameters.AddWithValue("@Adress", _adress);
        CMD.Parameters.AddWithValue("@Email", _email);
        CMD.Parameters.AddWithValue("@Postalcode", _postalcode);
        objData.ModifyData(CMD);
    }
}