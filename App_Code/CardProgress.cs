﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for CardProgress
/// </summary>
public class CardProgress
{
    public CardProgress(string _card)
    {
        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = _card;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion
    }
}