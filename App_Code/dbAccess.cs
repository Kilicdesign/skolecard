﻿using System.Configuration;  // giver adgangx til webconfig fil ved f.eks brug af "Connectionstrings"
using System.Data; // 
using System.Data.SqlClient; // denne skal med fordi vi bruger sql connections og sql commands. 

/// <Her bliver der refereret til nogle namespaces der er indbygget i frameworket>
/// Summary description for dbaccess
/// </summary>

public class dbAccess // Public class - her kan man få adgang til databasen uanset hvor i en fil man befinder sig
{
    readonly string _strDb = ConfigurationManager.ConnectionStrings["conn"].ConnectionString; // denne string og variabel giver adgang til "conn" (navngiv variabler med _)

    public DataTable GetData(SqlCommand cmd)
    {//Start getData
        
        DataSet objDs = new DataSet(); // objDs er en instans af et dataset - man kan kalde den noget andet. obj = objekt Ds = dataset / Dataset kan bestå af flere tables.
        SqlConnection objConn = new SqlConnection(_strDb); // obj - objekt connection her er det så _strDb der er vores kort og adgang til databasen.
        SqlDataAdapter objDa = new SqlDataAdapter(); // hvad er det vi har med at gøre SQL commands Adapteren skal anvendes hvis du vil bruge SQL commands
        
        cmd.Connection = objConn;
        objDa.SelectCommand = cmd;

        objDa.Fill(objDs); // her fylder vi vores dataadapter med vores dataset og efterfølgende bliver den lukket objConn.close på linje 23
        objConn.Close();

        return objDs.Tables[0]; // index starter med 0 så her er det første og eneste table den tager.
    }// Slut getData
	public void ModifyData(SqlCommand cmd) // denne publiv void styre manipulation af data. 
	{// Start Modify
        SqlConnection objConn = new SqlConnection(_strDb);
        cmd.Connection = objConn;
        objConn.Open();
        cmd.ExecuteNonQuery();
        objConn.Close(); // objekt close connection
    }// SlutModify
}