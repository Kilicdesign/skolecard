﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    UserFac objAddUser = new UserFac();
    protected void Page_Load(object sender, EventArgs e)
    {
        txtTest.Focus();
        pnlEmail.Visible = false;
    }
    
    protected void txtTest_TextChanged(object sender, EventArgs e)
    {

        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = txtTest.Text;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion

        txtTest.Text = "";
        //UDSKRIFT
        txtNavn.Text = fornavn;
        txtMellemNavn.Text = mellemnavn;
        txtEfterNavn.Text = efternavn;
        txtEfterNavn_2.Text = efternavn2;
        txtVejNavn.Text = vejnavn;
        txtHusNr.Text = husnr;
        txtAfdeling.Text = afdeling;
        txtEtage.Text = etage;
        txtSide.Text = side;
        txtPostNummer.Text =  end;
        litTest.Text = "Alt er læst";
    }
    
    protected void btnNext_Click(object sender, EventArgs e)
    {
        pnlEmail.Visible = true;
        pnlInfo.Visible = false;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objAddUser.AddUser(txtNavn.Text + " " + txtMellemNavn.Text + " " + txtEfterNavn.Text + " " + txtEfterNavn_2.Text, txtVejNavn.Text + " " + txtHusNr.Text + " " + txtAfdeling.Text + " " + txtEtage.Text + " " + txtSide.Text, txtEmail.Text, Convert.ToInt32(txtPostNummer.Text));
        Response.Redirect("/Admin/Default.aspx");
    }
}

