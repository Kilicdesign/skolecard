﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_FirstLvl : System.Web.UI.Page
{
    public int UserID { get; set; }
    FirstFac fac = new FirstFac();
    protected void Page_Load(object sender, EventArgs e)
    {
        //CARDPANEL
        cardPanel();
        txtCard.Focus();
        litScript.Visible = true;
        focusScript("#txtCard");
    }

    protected void txtCard_TextChanged(object sender, EventArgs e)
    {
        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = txtCard.Text;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion
        DataTable user = fac.FindUser(fornavn + " " + mellemnavn + " " + efternavn + " " + efternavn2, vejnavn + " " + husnr + " " + afdeling + " " + etage + " " + side);

        litScript.Visible=false;

        if (user.Rows.Count != 0)
        {
            DataRow TUser = user.Rows[0];
            var userLevel = Convert.ToInt32(TUser["fldUserLevel"]);

            DataRow ActiveStatusRow = fac.CheckActive();
            int ActiveStatus = Convert.ToInt32(ActiveStatusRow["fldActiveStatus"]);

            if (userLevel == 1)
            {
                //HVIS DET ER ADMIN
                adminPanel();
                //

                if (ActiveStatus == 1)
                {
                    litSystemStatus.Text = "<h4 class='success'>Systemet er aktiveret!</h4>";
                }
                else
                {
                    litSystemStatus.Text = "<h4 class='error'>Systemet er deaktiveret!</h4>";
                }
            }
            else
            {
                if (ActiveStatus == 1)
                {
                    //HVIS DET ER NORMAL BRUGER
                    userPanel();
                    int UserId = Convert.ToInt32(TUser["fldUserId"]);

                    DataTable log = fac.UserLogWithNul(UserId);


                    if (log.Rows.Count == 0)
                    {
                        btnUserLogInd.Text = "Start din tid nu";
                    }
                    else if (log.Rows.Count == 1)
                    {
                        DataRow userLog = log.Rows[0];
                        var userStartTime = Convert.ToDateTime(userLog["fldStartTime"]).ToShortDateString();
                        if (userStartTime == DateTime.Now.ToShortDateString())
                        {
                            btnUserLogInd.Text = "Gå hjem nu";
                        }
                        else if (userStartTime != DateTime.Now.ToShortDateString())
                        {
                            fac.UserNotLoggetOut(UserId);
                            btnUserLogInd.Text = "Start din tid nu";
                            litUserText.Text = "Du havde ikke logget ud";
                        }
                        else
                        {
                            btnUserLogInd.Text = "Start din tid nu";
                        }
                    }
                }
                else
                {
                    litMsg.Text = "<h4 class='error'>Systemet er deaktivt. Kontakt en administrator for at starte.</h4>";
                }

            }
        }
        else
        {
            litMsg.Text = "<h4 class='error'>Du er ikke aktiv, kontakt en administrator for at oprette dig.</h4>";
        }
    }

    // ***** //
    // ADMIN //
    // ***** //

    //INDEX
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        fac.setActive();
        adminPanel();
        litSystemStatus.Text = "<h4 class='success'>Du har nu aktiveret systemet!</h4>";
        btnDeactive(btnCreateUser);
        btnDeactive(btnListUser);
    }
    protected void btnDeactivate_Click(object sender, EventArgs e)
    {
        fac.setDeactive();
        adminPanel();
        litSystemStatus.Text = "<h4 class='error'>Du har nu deaktiveret systemet!</h4>";
        btnDeactive(btnCreateUser);
        btnDeactive(btnListUser);
    }
    protected void btnCreateUser_Click(object sender, EventArgs e)
    {
        createUserPanelStep1();
        btnActive(btnCreateUser);
        btnDeactive(btnListUser);
        txtTest.Focus();
        focusScript("#txtTest");
        litScript.Visible = true;
    }
    protected void btnListUser_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["user"]))
        {
            Response.Redirect(Request.Path);

        }
        adminUserList();
        btnActive(btnListUser);
        btnDeactive(btnCreateUser);

        litAdminListUser.Text = "<h2>Bruger oversigt</h2>";
        DataTable DTusers = fac.Users();
        foreach (DataRow user in DTusers.Rows)
        {

            UserID = Convert.ToInt32(user["fldUserId"]);
            DataTable logs = fac.UserLogs(UserID);
            if (logs.Rows.Count != 0)
            {
                DataRow userLog = logs.Rows[0];

                if (Convert.ToInt32(userLog["fldStatus"]) == 0 && Convert.ToDateTime(userLog["fldStartTime"]).ToLongDateString() == DateTime.Now.ToLongDateString())
                {
                    litAdminListUser.Text += "<i class='fa fa-check-circle fa-2x success' aria-hidden='true'></i>";
                    litAdminListUser.Text += "<a id='userslog' data-id=" + UserID + " >" + user["fldName"] + "</a><br />";
                }
                else
                {
                    litAdminListUser.Text += "<i class='fa fa-times-circle fa-2x error' aria-hidden='true'></i>";
                    litAdminListUser.Text += "<a id='userslog' data-id=" + UserID + " >" + user["fldName"] + "</a><br />";

                }
            }


            litAdminListUser.Text += "<div class='userslog-" + UserID + "' style='display:none'>";

            if (logs.Rows.Count == 0)
            {
                litAdminListUser.Text += user["fldName"] + " har ikke noget log";
            }
            foreach (DataRow log in logs.Rows)
            {
                if (Convert.ToInt32(log["fldStatus"]) == 1)
                {
                    litAdminListUser.Text += "<i class='fa fa-check-circle fa-2x success' aria-hidden='true'>";
                    litAdminListUser.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litAdminListUser.Text += "</i><br/>";
                }
                else if (Convert.ToInt32(log["fldStatus"]) == 0 && Convert.ToDateTime(log["fldStartTime"]).ToLongDateString() == DateTime.Now.ToLongDateString())
                {
                    litAdminListUser.Text += "<i class='fa fa-times-circle fa-2x error' aria-hidden='true'>";
                    litAdminListUser.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litAdminListUser.Text += "</i>(i dag)<br/>";
                }
                else
                {
                    litAdminListUser.Text += "<i class='fa fa-times-circle fa-2x error' aria-hidden='true'>";
                    litAdminListUser.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litAdminListUser.Text += "</i><br/>";

                }
            }
            litAdminListUser.Text += "</div>";
        }

  
    }
    //CREATE USER

    protected void txtTest_TextChanged(object sender, EventArgs e)
    {

        createUserPanelStep2();
        txtEmail.Focus();
        litScript.Visible = false;
        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = txtTest.Text;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion

        txtTest.Text = "";
        //UDSKRIFT
        txtNavn.Text = fornavn;
        txtMellemNavn.Text = mellemnavn;
        txtEfterNavn.Text = efternavn;
        txtEfterNavn_2.Text = efternavn2;
        txtVejNavn.Text = vejnavn;
        txtHusNr.Text = husnr;
        txtAfdeling.Text = afdeling;
        txtEtage.Text = etage;
        txtSide.Text = side;
        txtPostNummer.Text = end;
    }

    protected void btnCreateUserNext_Click(object sender, EventArgs e)
    {
        createUserPanelStep2();
    }

    protected void btnCreateUserSubmit_Click(object sender, EventArgs e)
    {
        //fac.AddUser("Harun", "Test", "test@test.dk", 8210);
        fac.AddUser(txtNavn.Text + " " + txtMellemNavn.Text + " " + txtEfterNavn.Text + " " + txtEfterNavn_2.Text, txtVejNavn.Text + " " + txtHusNr.Text + " " + txtAfdeling.Text + " " + txtEtage.Text + " " + txtSide.Text, txtEmail.Text, Convert.ToInt32(txtPostNummer.Text));
        
        litTest.Text = txtNavn.Text;
        litSystemStatus.Text = "<h4 class='success'>" + txtNavn.Text + " er oprettet.</h4>";
        adminPanel();

    }

    //BRUGERE



    //PANELS

    //ADMIN

    //INDEX
    private void adminPanel()
    {
        pnlAdmin.Visible = true;
        pnl10.Visible = false;
        pnlUser.Visible = false;
        pnlCreateUser.Visible = false;
    }
    private void adminUserList()
    {
        pnlAdmin.Visible = true;
        pnl10.Visible = false;
        pnlUser.Visible = false;
        pnlCreateUser.Visible = false;
        pnlListUser.Visible = true;
    }
    //CREATEUSER
    private void createUserPanelStep1()
    {
        pnlAdmin.Visible = true;
        pnl10.Visible = false;
        pnlUser.Visible = false;
        pnlCreateUser.Visible = true;
        pnlCreateUserStep2.Visible = false;
        pnlCreateUserStep1.Visible = true;
        pnlListUser.Visible = false;
    }
    //CREATEUSERSTEP2
    private void createUserPanelStep2()
    {
        pnlAdmin.Visible = true;
        pnl10.Visible = false;
        pnlUser.Visible = false;
        pnlCreateUser.Visible = true;
        pnlCreateUserStep2.Visible = true;
        pnlCreateUserStep1.Visible = true;
        btnCreateUserNext.Visible = false;
    }
    //USER 
    private void userPanel()
    {
        pnlAdmin.Visible = false;
        pnl10.Visible = false;
        pnlUser.Visible = true;
        pnlUserLog.Visible = false;
    }
    private void userLogPanel()
    {
        pnlAdmin.Visible = false;
        pnl10.Visible = false;
        pnlUser.Visible = true;
        pnlUserLog.Visible = true;
    }

    //LISTUSER


    //CARD
    private void cardPanel()
    {
        pnlAdmin.Visible = false;
        pnl10.Visible = true;
        pnlUser.Visible = false;
    }

    //MISC
    private void focusScript(string id)
    {
        litScript.Text =
            @"<script>
                $(document).ready(function() {
                    $('" + id + @"').blur(function() {
                        setTimeout(function() {
                            $('" + id + @"').focus();
                        },0)
                    });
                });
            </script>";
    }
    private void btnActive(Button btnName)
    {
        btnName.CssClass = "btn btn-primary btn-lg btn-active";
    }
    private void btnDeactive(Button btnName)
    {
        btnName.CssClass = "btn btn-primary btn-lg";
    }

    protected void btnUserLogInd_Click(object sender, EventArgs e)
    {
        userPanel();
        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = txtCard.Text;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion
        DataRow user = fac.FindUser(fornavn + " " + mellemnavn + " " + efternavn + " " + efternavn2, vejnavn + " " + husnr + " " + afdeling + " " + etage + " " + side).Rows[0];
        int userId = Convert.ToInt32(user["fldUserId"]);

        if (btnUserLogInd.Text == "Gå hjem nu")
        {
            btnUserLogInd.Visible = false;
            litUserText.Text = "Ja gå du bare hjem, du bliver logget af alligevel.";
            //btnUserLogInd.Text = "Start din tid nu";
            fac.EndUserLog(userId);
            Response.AddHeader("REFRESH", "4;URL=" + Request.RawUrl);
        }
        else if (btnUserLogInd.Text == "Start din tid nu")
        {
            litUserText.Text = "Din tid er startet";
            btnUserLogInd.Text = "Gå hjem nu";
            fac.CreateUserLog(userId);
        }

    }

    protected void btnUserLog_Click(object sender, EventArgs e)
    {
        #region CARD
        //TEXTTRIM REPLACE ETC
        var Card = txtCard.Text;

        //Hvis text har mere end 2 mellemrum
        Regex regex = new Regex(@"\s{2,}");
        if (regex.IsMatch(Card))
        {
            Card = regex.Replace(Card, ".").Replace("'", "Ø").Replace("¨", "Å").Replace("å", "Æ");
        }
        //Splitter navn og add
        string[] split = Card.Split('.');
        //Fjerner special tegn og replacer med komma
        var navn = split[0].Replace("&", " ").Replace("%", string.Empty).Replace(" ", ",");
        //Separerer navnene
        string[] navnArr = navn.Split(',');
        //Navn props
        var efternavn = navnArr[0];
        var efternavn2 = string.Empty;
        var fornavn = navnArr[1];
        var mellemnavn = string.Empty;
        if (navnArr.Length > 2)
        {
            mellemnavn = navnArr[2];
        }
        if (navnArr.Length > 3)
        {
            efternavn2 = navnArr[3];
        }

        //ADD SPLIT
        var add = split[1];
        string[] addArr = add.Replace(" ", "-").Split('-');
        var vejnavn = addArr[0];
        var husnr = addArr[1];

        //ARRAY PROPS
        var afdeling = string.Empty;
        var etage = string.Empty;
        var side = string.Empty;
        if (addArr.Length > 4)
        {
            afdeling = addArr[2];
            etage = addArr[3];
            side = addArr[4];
        }
        else if (addArr.Length > 3)
        {
            etage = addArr[2];
            side = addArr[3];
        }
        else if (addArr.Length > 2)
        {
            etage = addArr[2];
        }

        //POSTNR
        var postString = split[2];
        var end = postString.Substring(3, 4);
        #endregion
        DataRow user = fac.FindUser(fornavn + " " + mellemnavn + " " + efternavn + " " + efternavn2, vejnavn + " " + husnr + " " + afdeling + " " + etage + " " + side).Rows[0];
        int userId = Convert.ToInt32(user["fldUserId"]);
        if (btnUserLog.Text == "Se din log")
        {
            userLogPanel();
            DataTable DTLog = fac.UserLogs(userId);

            litUserLog.Text = "<h2>Dine logs er her: " + fornavn + "</h2>";
            foreach (DataRow log in DTLog.Rows)
            {
                if (Convert.ToInt32(log["fldStatus"]) == 1)
                {
                    litUserLog.Text += "<i class='fa fa-check-circle fa-2x success' aria-hidden='true'>";
                    litUserLog.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litUserLog.Text += "</i><br/>";
                }
                else if (Convert.ToInt32(log["fldStatus"]) == 0 && Convert.ToDateTime(log["fldStartTime"]).ToLongDateString() == DateTime.Now.ToLongDateString())
                {
                    litUserLog.Text += "<i class='fa fa-times-circle fa-2x error' aria-hidden='true'>";
                    litUserLog.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litUserLog.Text += "</i>(i dag)<br/>";
                }
                else
                {
                    litUserLog.Text += "<i class='fa fa-times-circle fa-2x error' aria-hidden='true'>";
                    litUserLog.Text += Convert.ToDateTime(log["fldStartTime"]).ToLongDateString();
                    litUserLog.Text += "</i><br/>";

                }
            }


            btnUserLog.Text = "Minimer log";
        }
        else
        {
            btnUserLog.Text = "Se din log";
            userPanel();
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
}