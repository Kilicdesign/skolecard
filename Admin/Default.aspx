﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BackEndMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:Panel ID="pnlInfo" runat="server">
    <asp:TextBox ID="txtTest"
        runat="server"
        AutoPostBack="true"
        OnTextChanged="txtTest_TextChanged"
        Style="width: 0; overflow: hidden; opacity: 0;"
        ClientIDMode="Static" />
    <asp:TextBox ID="txtClick"
        runat="server"
        Style="width: 0; overflow: hidden; opacity: 0;"
        ClientIDMode="Static" /><br />
    <div class="page-header">
        <h2>Informationer</h2>
    </div>
        <div class="col-md-4">
            Navn:
        <asp:TextBox ID="txtNavn" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Mellemnavn:
        <asp:TextBox ID="txtMellemNavn" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Efternavn:
        <asp:TextBox ID="txtEfterNavn" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Efternavn2:
        <asp:TextBox ID="txtEfterNavn_2" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            <div class="page-header">
                <h2>Adresse</h2>
            </div>
            Vejnavn:
        <asp:TextBox ID="txtVejNavn" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Husnr:
        <asp:TextBox ID="txtHusNr" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Afdeling:
        <asp:TextBox ID="txtAfdeling" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Etage:
        <asp:TextBox ID="txtEtage" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Side:
        <asp:TextBox ID="txtSide" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            Postnummer:
        <asp:TextBox ID="txtPostNummer" CssClass="form-control" runat="server" ReadOnly="true" /><br />
            <%--    <asp:TextBox ID="txtMail"
        ClientIDMode="Static"
        runat="server"  /><br />--%>
            <asp:Literal ID="litTest" runat="server" /><br />
        </div>
        <div class="form-group">
            <asp:Button ID="btnNext" CssClass="btn btn-default" OnClick="btnNext_Click" runat="server" Text="next" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEmail" runat="server">
        <div class="col-md-4">
            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" />
            <asp:Button ID="btnSubmit" CssClass="btn btn-default" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </div>
    </asp:Panel>
</asp:Content>

