﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BackEndMasterPage.master" AutoEventWireup="true" CodeFile="FirstLvl.aspx.cs" Inherits="Admin_FirstLvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <asp:Literal ID="litScript" Visible="false" runat="server" />
    <asp:Literal ID="litUserScript" runat="server" />
    <script>$(document).ready(function () {
                $('a#userslog').on('click', function () {
                    var id = $(this).data('id');
                    $('div.userslog-' + id).toggle();
                });
            });</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnl10" runat="server">
        <h1>Kør dit kort igennem.</h1>
        <i class="fa fa-credit-card fa-5x" aria-hidden="true"></i>
        <asp:TextBox ID="txtCard"
                runat="server"
                AutoPostBack="true"
                OnTextChanged="txtCard_TextChanged"
                Style="width: 0; overflow: hidden; opacity: 0;"
                ClientIDMode="Static" />
        <asp:Literal ID="litMsg" runat="server" />

    </asp:Panel>



    <asp:Panel ID="pnlAdmin" runat="server">
        <div class="text-center">
            <h1>Hej Admin</h1>
            <asp:Literal ID="litSystemStatus" runat="server" />
            <div class="row">
                <div class="col-md-6 text-right">
                    <asp:Button ID="btnActivate" CssClass="btn btn-success btn-lg" Text="Aktiver systemet" OnClick="btnActivate_Click" runat="server" />
                </div>
                <div class="col-md-6 text-left">
                    <asp:Button ID="btnDeactivate" CssClass="btn btn-danger btn-lg" Text="Deaktiver systemet" OnClick="btnDeactivate_Click" runat="server" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 text-right">
                    <asp:Button ID="btnCreateUser" CssClass="btn btn-primary btn-lg" Text="Tilføj ny bruger" OnClick="btnCreateUser_Click" runat="server" />
                </div>
                <div class="col-md-6 text-left">
                    <asp:Button ID="btnListUser" CssClass="btn btn-primary btn-lg" Text="Se brugere" OnClick="btnListUser_Click" runat="server" />
                </div>
            </div>
        </div>

        <!--CREATEUSER PANEL-->
        <asp:Panel ID="pnlCreateUser" runat="server">
            <asp:TextBox ID="txtTest"
                runat="server"
                AutoPostBack="true"
                Style="width: 0; overflow: hidden; opacity: 0;"
                OnTextChanged="txtTest_TextChanged"
                ClientIDMode="Static" />

            <asp:TextBox ID="txtClick"
                runat="server"
                Style="width: 0; overflow: hidden; opacity: 0;"
                ClientIDMode="Static" />
            <!-- CREATEUSER INFO PANEL -->
            <asp:Panel ID="pnlCreateUserStep1" runat="server">

                <div class="form-horizontal">
                    <div class="page-header">
                        <h2>Informationer</h2>
                    </div>
                    <!-- Text input-->
                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Navn" AssociatedControlID="txtNavn" runat="server" />
                            <asp:TextBox ID="txtNavn" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                        <div class="col-md-5">
                            <asp:Label Text="Mellem navn" AssociatedControlID="txtMellemNavn" runat="server" />
                            <asp:TextBox ID="txtMellemNavn" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                    </div>

                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Efternavn" AssociatedControlID="txtEfterNavn" runat="server" />
                            <asp:TextBox ID="txtEfterNavn" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                        <div class="col-md-5">
                            <asp:Label Text="Efternavn2" AssociatedControlID="txtEfterNavn_2" runat="server" />
                            <asp:TextBox ID="txtEfterNavn_2" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                    </div>

                    <div class="clearfix"></div>


                    <div class="page-header">
                        <h2>Adresse</h2>
                    </div>
                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Vejnavn" AssociatedControlID="txtVejNavn" runat="server" />
                            <asp:TextBox ID="txtVejNavn" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                        <div class="col-md-5">
                            <asp:Label Text="Husnr" AssociatedControlID="txtHusNr" runat="server" />
                            <asp:TextBox ID="txtHusNr" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                    </div>
                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Afdeling" AssociatedControlID="txtAfdeling" runat="server" />
                            <asp:TextBox ID="txtAfdeling" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                        <div class="col-md-5">
                            <asp:Label Text="Etage" AssociatedControlID="txtEtage" runat="server" />
                            <asp:TextBox ID="txtEtage" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                    </div>
                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Side" AssociatedControlID="txtSide" runat="server" />
                            <asp:TextBox ID="txtSide" CssClass="form-control" runat="server" readonly="true"/>
                        </div>
                        <div class="col-md-5">
                            <asp:Label Text="Postnummer" AssociatedControlID="txtPostNummer" runat="server" />
                            <asp:TextBox ID="txtPostNummer" CssClass="form-control" runat="server" readonly="true" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="form-group form-group-lg col-md-push-2 col-md-5">
                        <asp:Button ID="btnCreateUserNext" Style="width: 0; overflow: hidden; opacity: 0;" OnClick="btnCreateUserNext_Click" runat="server" Text="Videre" />
                    </div>

                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCreateUserStep2" Visible="false" runat="server">
                <div class="page-header">
                    <h2>Email</h2>
                </div>

                <div class="form-horizontal">
                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <div class="col-md-5">
                            <asp:Label Text="Email" AssociatedControlID="txtEmail" runat="server" />
                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" />
                        </div>
                    </div>

                    <div class="form-group form-group-lg col-md-10 col-md-push-2">
                        <asp:Button ID="btnCreateUserSubmit" CssClass="btn btn-success btn-lg" runat="server" Text="Opret" OnClick="btnCreateUserSubmit_Click" />
                    </div>
                </div>
                    <asp:Literal ID="litTest" runat="server" />
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="pnlListUser" Visible="false" runat="server">  
            <asp:Literal ID="litAdminListUser" runat="server" />
        </asp:Panel>

    </asp:Panel>


    <asp:Panel ID="pnlUser" runat="server">
        <h1>Hej User</h1>
        <asp:Button Text="Start tid" ID="btnUserLogInd" OnClick="btnUserLogInd_Click" runat="server" />
        <asp:Button Text="Se din log" ID="btnUserLog" OnClick="btnUserLog_Click" runat="server" />
        <asp:Button Text="Log ud" ID="btnLogout" OnClick="btnLogout_Click" runat="server" />
        <asp:Literal ID="litUserText" runat="server" />
        <asp:Panel ID="pnlUserLog" Visible="false" runat="server">
            <asp:Literal ID="litUserLog" runat="server" />
            <asp:Literal ID="litUserNotLog" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

